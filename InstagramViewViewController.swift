//
//  InstagramViewViewController.swift
//  RiotGenerator
//
//  Created by lamplight02 on 2017/03/21.
//  Copyright © 2017年 Mac. All rights reserved.
//

import UIKit
import Photos

class InstagramViewViewController: UIViewController {
    
    
    
    @IBOutlet weak var InstagramViewView: UIView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        InstagramViewView.load(a1)
        
        self.InstagramViewView.addSubview(a1)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func InstagramButton(_ sender: UIButton) {
        
        var imageIdentifier: String?
        PHPhotoLibrary.shared().performChanges({ () -> Void in
            let createAssetRequest = PHAssetChangeRequest.creationRequestForAsset(from: self.view.image!)
            let placeHolder = createAssetRequest.placeholderForCreatedAsset
            imageIdentifier = placeHolder!.localIdentifier
        }, completionHandler: { (success, error) -> Void in
            print("Finished adding asset.\(success ? "success" : "error")")
            let testURL = URL(string: "instagram://library?LocalIdentifier=" + imageIdentifier!)
            if UIApplication.shared.canOpenURL(testURL!) {
                UIApplication.shared.openURL(testURL!)
            }
        })
        
        
    }
  

}
