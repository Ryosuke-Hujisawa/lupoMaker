# Lamplight-BoudouMaker-iOS

## Description this app

Designed a book cover. Imagine the title of the cover of the book. Share your sense of imagination to SNS. Only you book cover.

## technology

This is my portfolio proof of Swift skill. I made it when I was 18 and than released App store.

- Swift3 
- Swift4 
- Xcode


![alt text](http://wordtranslate.info/img/lupo02.png)

## Screen Transition

Simple screen transition. Abundant template cover. Sharing function to SNS.


![alt text](http://wordtranslate.info/img/lupo03.png)


## publication

- [GameWith](https://gamewith.jp/gamedb/show/1906?from=ios)

- [Applinote](https://applinote.com/ios/app/178692/screenshot)
