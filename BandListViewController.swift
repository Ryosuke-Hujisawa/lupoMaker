

import UIKit

class BandListViewController: UIViewController {

    
    
    // インジケータのインスタンス
    let indicator = UIActivityIndicatorView()

    @IBOutlet weak var BookImage01: UIImageView!
    @IBOutlet weak var BookImage02: UIImageView!
    @IBOutlet weak var BookImage03: UIImageView!
    @IBOutlet weak var BookImage04: UIImageView!
    @IBOutlet weak var BookImage05: UIImageView!
    @IBOutlet weak var BookImage06: UIImageView!
    @IBOutlet weak var BookImage07: UIImageView!
    @IBOutlet weak var BookImage08: UIImageView!
    

    override func viewDidLoad() {
        super.viewDidLoad()


        self.BookImage01.layer.cornerRadius = 30
        self.BookImage01.layer.masksToBounds = true
        
        self.BookImage02.layer.cornerRadius = 30
        self.BookImage02.layer.masksToBounds = true

        self.BookImage03.layer.cornerRadius = 30
        self.BookImage03.layer.masksToBounds = true

        self.BookImage04.layer.cornerRadius = 30
        self.BookImage04.layer.masksToBounds = true
        
        self.BookImage05.layer.cornerRadius = 30
        self.BookImage05.layer.masksToBounds = true

        self.BookImage06.layer.cornerRadius = 30
        self.BookImage06.layer.masksToBounds = true
        
        self.BookImage07.layer.cornerRadius = 30
        self.BookImage07.layer.masksToBounds = true

        self.BookImage08.layer.cornerRadius = 30
        self.BookImage08.layer.masksToBounds = true

//        // 背景画像01
//        UIGraphicsBeginImageContext(self.view.frame.size)
//        UIImage(named: "back-1")?.draw(in: self.view.bounds)
//        let image: UIImage! = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
//        self.view.backgroundColor = UIColor(patternImage: image)


        BookImage01.isUserInteractionEnabled = true
        BookImage01.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.saveImage(_:))))
        BookImage01.center = self.view.center
        BookImage02.isUserInteractionEnabled = true
        BookImage02.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.saveImage(_:))))
        BookImage02.center = self.view.center
        BookImage03.isUserInteractionEnabled = true
        BookImage03.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.saveImage(_:))))
        BookImage03.center = self.view.center
        BookImage04.isUserInteractionEnabled = true
        BookImage04.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.saveImage(_:))))
        BookImage04.center = self.view.center
        BookImage05.isUserInteractionEnabled = true
        BookImage05.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.saveImage(_:))))
        BookImage05.center = self.view.center
        BookImage07.isUserInteractionEnabled = true
        BookImage07.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.saveImage(_:))))
        BookImage07.center = self.view.center
        BookImage08.isUserInteractionEnabled = true
        BookImage08.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.saveImage(_:))))
        BookImage08.center = self.view.center

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    func ImageProcessingIndicator(){

        indicator.activityIndicatorViewStyle = .whiteLarge
        indicator.center = self.view.center
        indicator.color = UIColor.green
        indicator.hidesWhenStopped = true
        self.view.addSubview(indicator)
        self.view.bringSubview(toFront: indicator)
        indicator.startAnimating()
    }


    @IBAction func mosaic(_ sender: Any) {
 
        ImageProcessingIndicator()
        let dispatchTime: DispatchTime = DispatchTime.now() + Double(Int64(1.0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
            
            let array = ["syouzyou", "iten2", "iten", "last", "soccer", "base","syousetu","sinu"]
            for i in 0 ..< array.count {
                let photo = UIImage(named: array[i])
                let ciPhoto = CIImage(cgImage: (photo?.cgImage)!)
                let filter = CIFilter(name: "CIPixellate")
                filter?.setValue(ciPhoto, forKey: kCIInputImageKey)
                filter?.setValue(200, forKey: "inputScale")
                let filteredImage:CIImage = (filter?.outputImage)!
                let ciContext:CIContext = CIContext(options: nil)
                let imageRef = ciContext.createCGImage(filteredImage, from: filteredImage.extent)
                let outputImage = UIImage(cgImage:imageRef!, scale:1.0, orientation:UIImageOrientation.up)
                
                if(photo == UIImage(named: "syouzyou")){
                    self.BookImage08.image = outputImage
                }
                if(photo == UIImage(named: "iten2")){
                    self.BookImage03.image = outputImage
                }
                if(photo == UIImage(named: "iten")){
                    self.BookImage02.image = outputImage
                }
                if(photo == UIImage(named: "last")){
                    self.BookImage04.image = outputImage
                }
                if(photo == UIImage(named: "soccer")){
                    self.BookImage06.image = outputImage
                }
                if(photo == UIImage(named: "base")){
                    self.BookImage01.image = outputImage
                }
                if(photo == UIImage(named: "syousetu")){
                    self.BookImage07.image = outputImage
                }
                if(photo == UIImage(named: "sinu")){
                    self.BookImage05.image = outputImage
                    // 0秒後にアニメーションを停止させる
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0, execute: {
                        self.indicator.stopAnimating()
                    })
                }
            }

        })
        
        
            }

    @IBAction func monochrome(_ sender: Any) {
        
        ImageProcessingIndicator()
        let dispatchTime: DispatchTime = DispatchTime.now() + Double(Int64(1.0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {

                let array = ["syouzyou", "iten2", "iten", "last", "soccer", "base","syousetu","sinu"]

                for i in 0 ..< array.count {
        let myImage = UIImage(named: array[i])
        let ciImage: CIImage = CIImage(image: myImage!)!
        let ciFilter: CIFilter = CIFilter(name: "CIColorMonochrome")!
        ciFilter.setValue(ciImage, forKey: kCIInputImageKey)
        ciFilter.setValue(CIColor(red: 0.75, green: 0.75, blue: 0.75), forKey: "inputColor")
        ciFilter.setValue(1.0, forKey: "inputIntensity")
        let ciContext: CIContext = CIContext(options: nil)
        let cgimg: CGImage = ciContext.createCGImage(ciFilter.outputImage!, from: (ciFilter.outputImage?.extent)!)!
        let afterImage: UIImage = UIImage(cgImage: cgimg, scale: 1.0, orientation: UIImageOrientation.up)
                        if(myImage == UIImage(named: "syouzyou")){
                            self.BookImage08.image = afterImage
                        }
                        if(myImage == UIImage(named: "iten2")){
                            self.BookImage03.image = afterImage
                        }
                        if(myImage == UIImage(named: "iten")){
                            
                            self.BookImage02.image = afterImage
                            print("BookImage02")
                            
                        }
                        if(myImage == UIImage(named: "last")){
                            
                            self.BookImage04.image = afterImage
                            print("BookImage04")
                            
                        }
                        if(myImage == UIImage(named: "soccer")){
                            
                            self.BookImage06.image = afterImage
                            print("BookImage06")
                            
                        }
                        if(myImage == UIImage(named: "base")){
                            
                            self.BookImage01.image = afterImage
                            print("BookImage01")
                            
                        }
                        if(myImage == UIImage(named: "syousetu")){
                            
                            self.BookImage07.image = afterImage
                            print("BookImage07")
                        }
                        if(myImage == UIImage(named: "sinu")){
                            
                            self.BookImage05.image = afterImage
                            print("BookImage05")
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0, execute: {
                                self.indicator.stopAnimating()
                            })
                            
                        }
            }
        })

    }
    
    
    
    
    @IBAction func Sepia(_ sender: Any) {
        ImageProcessingIndicator()
        let dispatchTime: DispatchTime = DispatchTime.now() + Double(Int64(1.0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {

        let array = ["syouzyou", "iten2", "iten", "last", "soccer", "base","syousetu","sinu"]
        for i in 0 ..< array.count {
                var photo = UIImage(named: array[i])
                let ciPhoto = CIImage(cgImage: (photo?.cgImage)!)
                let filter = CIFilter(name: "CISepiaTone")
                filter?.setValue(ciPhoto, forKey: kCIInputImageKey)
                filter?.setValue(1.0, forKey: kCIInputIntensityKey)
                let filteredImage:CIImage = (filter?.outputImage)!
                let ciContext:CIContext = CIContext(options: nil)
                let imageRef = ciContext.createCGImage(filteredImage, from: filteredImage.extent)
                let outputImage = UIImage(cgImage:imageRef!, scale:1.0, orientation:UIImageOrientation.up)
                
                if(photo == UIImage(named: "syouzyou")){
                    
                    self.BookImage08.image = outputImage
                    
                }
                if(photo == UIImage(named: "iten2")){
                    
                    self.BookImage03.image = outputImage
                    
                }
                if(photo == UIImage(named: "iten")){
                    
                    self.BookImage02.image = outputImage
                    
                }
                if(photo == UIImage(named: "last")){
                    
                    self.BookImage04.image = outputImage
                    
                }
                if(photo == UIImage(named: "soccer")){
                    
                    self.BookImage06.image = outputImage
                    
                }
                if(photo == UIImage(named: "base")){
                    
                    self.BookImage01.image = outputImage
                    
                }
                if(photo == UIImage(named: "syousetu")){
                    
                    self.BookImage07.image = outputImage
                    
                }
                if(photo == UIImage(named: "sinu")){
                    
                    self.BookImage05.image = outputImage
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0, execute: {
                        self.indicator.stopAnimating()
                    })
                    
                }
        }
            
        })
 
    }
    
    
    
    
    
    
    func test08(){
        let xPosition = BookImage08.frame.origin.x + CGFloat(1000)
        print(xPosition)
        let yPosition = BookImage08.frame.origin.y
        print(yPosition)
        let height = BookImage08.frame.size.height
        let width = BookImage08.frame.size.width
        UIView.animate(withDuration: 0.5, animations: {
            self.BookImage08.frame = CGRect(x:xPosition, y:yPosition, width:width, height:height)
        })
    }
    func test03(){
        let xPosition = BookImage03.frame.origin.x + CGFloat(1000)
        print(xPosition)
        let yPosition = BookImage03.frame.origin.y
        print(yPosition)
        let height = BookImage03.frame.size.height
        let width = BookImage03.frame.size.width
        UIView.animate(withDuration: 1.0, animations: {
            self.BookImage03.frame = CGRect(x:xPosition, y:yPosition, width:width, height:height)
        })
    }
    func test02(){
        let xPosition = BookImage02.frame.origin.x + CGFloat(1000)
        print(xPosition)
        let yPosition = BookImage02.frame.origin.y
        print(yPosition)
        let height = BookImage02.frame.size.height
        let width = BookImage02.frame.size.width
        
        UIView.animate(withDuration: 1.5, animations: {
            self.BookImage02.frame = CGRect(x:xPosition, y:yPosition, width:width, height:height)
        })
    }
    func test04(){
        let xPosition = BookImage04.frame.origin.x + CGFloat(1000)
        print(xPosition)
        let yPosition = BookImage04.frame.origin.y
        print(yPosition)
        let height = BookImage04.frame.size.height
        let width = BookImage04.frame.size.width
        
        UIView.animate(withDuration: 2.0, animations: {
            self.BookImage04.frame = CGRect(x:xPosition, y:yPosition, width:width, height:height)
        })
    }
    func test06(){
        let xPosition = BookImage06.frame.origin.x + CGFloat(1000)
        print(xPosition)
        let yPosition = BookImage06.frame.origin.y
        print(yPosition)
        let height = BookImage06.frame.size.height
        let width = BookImage06.frame.size.width
        UIView.animate(withDuration: 2.5, animations: {
            self.BookImage06.frame = CGRect(x:xPosition, y:yPosition, width:width, height:height)
        })
    }
    func test01(){
        let xPosition = BookImage01.frame.origin.x + CGFloat(1000)
        print(xPosition)
        let yPosition = BookImage01.frame.origin.y
        print(yPosition)
        let height = BookImage01.frame.size.height
        let width = BookImage01.frame.size.width
        
        UIView.animate(withDuration: 3.0, animations: {
            self.BookImage01.frame = CGRect(x:xPosition, y:yPosition, width:width, height:height)
        })
    }
    func test07(){
        let xPosition = BookImage07.frame.origin.x + CGFloat(1000)
        print(xPosition)
        let yPosition = BookImage07.frame.origin.y
        print(yPosition)
        let height = BookImage07.frame.size.height
        let width = BookImage07.frame.size.width
        UIView.animate(withDuration: 3.5, animations: {
            self.BookImage07.frame = CGRect(x:xPosition, y:yPosition, width:width, height:height)
        })
    }
    func test05(){
        let xPosition = BookImage05.frame.origin.x + CGFloat(1000)
        print(xPosition)
        let yPosition = BookImage05.frame.origin.y
        print(yPosition)
        let height = BookImage05.frame.size.height
        let width = BookImage05.frame.size.width
        UIView.animate(withDuration: 4.0, animations: {
            self.BookImage05.frame = CGRect(x:xPosition, y:yPosition, width:width, height:height)
        })
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        test01()
        test02()
        test03()
        test04()
        test05()
        test06()
        test07()
        test08()
    }
 
    @IBAction func BackVireButton(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)

    }

    func saveImage(_ sender: UITapGestureRecognizer) {

        let targetImageView = sender.view! as! UIImageView
        let targetImage = targetImageView.image!
        UIImageWriteToSavedPhotosAlbum(targetImage, self, #selector(self.showResultOfSaveImage(_:didFinishSavingWithError:contextInfo:)), nil)
    }

    func showResultOfSaveImage(_ image: UIImage, didFinishSavingWithError error: NSError!, contextInfo: UnsafeMutableRawPointer) {
        
        var title = "保存完了"
        var message = "カメラロールに保存しました"
        
        if error != nil {
            title = "エラー"
            message = "保存に失敗しました"
        }
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
