import UIKit
import GoogleMobileAds
import Photos
var soundId:SystemSoundID = 0



var globalSelectedImageViewIndex:UIImage = UIImage(named:"animation1")!

class ViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var GeneratorEditButton: UIButton!
    @IBOutlet weak var RightGeneratorEditButton: UIImageView!
    @IBOutlet weak var LeftGeneratorEditButton: UIImageView!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var skyImageView: UIImageView!
    @IBOutlet var lawnsImageView: UIImageView!    
    @IBOutlet weak var bannerView: GADBannerView!

    var images = [UIImageView]()
    var scrollViewXValue  :CGFloat?
    var contentXValue  :CGFloat = 0.0
    var selectedImageViewIndex  = 0
    var currentScrollContentOffset  = CGPoint(x: 0.0, y: 0.0)
    var inDraggingProcess = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        BalloonAnimation()
        
        bannerView.adUnitID = "ca-app-pub-1369191412820443/3972986615"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        scrollView.delegate = self

        let directions: [UISwipeGestureRecognizerDirection] = [.right, .left]
        for direction in directions {
            let imageGesture = UISwipeGestureRecognizer(target: self, action: #selector(ViewController.handleSwipe(_:)))
            imageGesture.direction = direction
            let secondImageGesture = UISwipeGestureRecognizer(target: self, action: #selector(ViewController.handleSwipe(_:)))
            secondImageGesture.direction = direction

            let scrollGesture = UISwipeGestureRecognizer(target: self, action: #selector(ViewController.handleSwipe(_:)))
            scrollGesture.direction = direction

            skyImageView.isUserInteractionEnabled = true
            skyImageView.addGestureRecognizer(scrollGesture)

            lawnsImageView.isUserInteractionEnabled = true
            lawnsImageView.addGestureRecognizer(secondImageGesture)

            scrollView.isUserInteractionEnabled = true
            scrollView.addGestureRecognizer(imageGesture)
   
            let tap = UITapGestureRecognizer(target: self, action: #selector(ViewController.handleTap(_:)))
            scrollView.addGestureRecognizer(tap)
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if (sound == "あり"){
            print("効果音１")
            // システムサウンドへのパスを指定
            if let soundUrl:NSURL = NSURL(fileURLWithPath: "/Users/ryosukehujisawa/Desktop/BookCoverApp/sound/top.mp3") {
                // SystemsoundIDを作成して再生実行
                AudioServicesCreateSystemSoundID(soundUrl, &soundId)
                AudioServicesPlaySystemSound(soundId)
            }
        }
    }
        
        
       
    
    

    @IBAction func EditGeneratorButton(_ sender: UIButton) {

        if(selectedImageViewIndex + 0 == 0){
            globalSelectedImageViewIndex = UIImage(named:"animation1")!
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainVC = storyboard.instantiateViewController(withIdentifier: "RoitEditViewController01") as! RoitEditViewController01
            mainVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            self.present(mainVC, animated: true, completion: nil)
        }else{
        }
            if(selectedImageViewIndex + 0 == 1){
                globalSelectedImageViewIndex = UIImage(named:"animation2")!
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let mainVC = storyboard.instantiateViewController(withIdentifier: "RoitEditViewController01") as! RoitEditViewController01
                mainVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                self.present(mainVC, animated: true, completion: nil)
        }
        if(selectedImageViewIndex + 0 == 2){
            globalSelectedImageViewIndex = UIImage(named:"animation3")!
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let mainVC = storyboard.instantiateViewController(withIdentifier: "RoitEditViewController01") as! RoitEditViewController01
            mainVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            self.present(mainVC, animated: true, completion: nil)
        }
        if(selectedImageViewIndex + 0 == 3){
            globalSelectedImageViewIndex = UIImage(named:"animation4")!
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let mainVC = storyboard.instantiateViewController(withIdentifier: "RoitEditViewController01") as! RoitEditViewController01
            mainVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            self.present(mainVC, animated: true, completion: nil)
        }
        if(selectedImageViewIndex + 0 == 4){
            globalSelectedImageViewIndex = UIImage(named:"animation5")!
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let mainVC = storyboard.instantiateViewController(withIdentifier: "RoitEditViewController01") as! RoitEditViewController01
            mainVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            self.present(mainVC, animated: true, completion: nil)
        }
        if(selectedImageViewIndex + 0 == 5){
            globalSelectedImageViewIndex = UIImage(named:"animation6")!
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let mainVC = storyboard.instantiateViewController(withIdentifier: "RoitEditViewController01") as! RoitEditViewController01
            mainVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            self.present(mainVC, animated: true, completion: nil)
        }
        if(selectedImageViewIndex + 0 == 6){
            globalSelectedImageViewIndex = UIImage(named:"animation7")!
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let mainVC = storyboard.instantiateViewController(withIdentifier: "RoitEditViewController01") as! RoitEditViewController01
            mainVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            self.present(mainVC, animated: true, completion: nil)
        }
       }

    override func viewDidAppear(_ animated: Bool) {

        for i in 1...6{
            let image = UIImage(named: "animation\(i%7 != 0 ? i%7 : 1)")
            let imageView = UIImageView.init(image: image)
            images.append(imageView)
            scrollViewXValue = scrollView.frame.width/2.0  + scrollView.frame.width * CGFloat (i - 1)
            contentXValue += scrollViewXValue!
            imageView.frame = CGRect(x: scrollViewXValue! - 50 , y: scrollView.frame.height/2.0 - 100 , width: 120, height: 200)
            scrollView.addSubview(imageView)
        }

        scrollView.contentSize = CGSize(width: contentXValue, height: scrollView.frame.height)
        scrollView.setContentOffset(CGPoint( x: scrollView.contentOffset.x + scrollView.frame.width * CGFloat(self.images.count/2), y: self.scrollView.frame.origin.y), animated: false)
        scrollView.clipsToBounds = false
        self.selectedImageViewIndex = images.count/2
        let midImageView = images[selectedImageViewIndex]
        midImageView.frame = CGRect(x: midImageView.frame.origin.x, y: midImageView.frame.origin.y, width: midImageView.frame.size.width + 30.0 , height: midImageView.frame.size.height + 30.0)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

 
    
    func handleSwipe(_ sender: UISwipeGestureRecognizer) {
        if (sound == "あり"){
            print("効果音１")
            // システムサウンドへのパスを指定
            if let soundUrl:NSURL = NSURL(fileURLWithPath: "/Users/ryosukehujisawa/Desktop/BookCoverApp/sound/top.mp3") {
                // SystemsoundIDを作成して再生実行
                AudioServicesCreateSystemSoundID(soundUrl, &soundId)
                AudioServicesPlaySystemSound(soundId)
            }
            
        }
        

        
    
        if (sender.direction.rawValue == 2 ){
        
            if selectedImageViewIndex != images.count - 1  {
                
                scrollView.setContentOffset(CGPoint( x: scrollView.contentOffset.x + scrollView.frame.width , y: self.scrollView.frame.origin.y), animated: true)
                selectedImageViewIndex += 1
                
                print(selectedImageViewIndex)
                print("selectedImageViewIndex")
                
                
                let nextImageView = images[selectedImageViewIndex]
                nextImageView.frame = CGRect(x: nextImageView.frame.origin.x, y: nextImageView.frame.origin.y, width: nextImageView.frame.size.width + 30.0 , height: nextImageView.frame.size.height + 30.0)
                let previousImageView = images[selectedImageViewIndex-1]
                
                previousImageView.frame = CGRect(x: previousImageView.frame.origin.x, y: previousImageView.frame.origin.y, width: previousImageView.frame.size.width - 30.0 , height: previousImageView.frame.size.height - 30.0)
            }
            

        }else{
            
            if  selectedImageViewIndex != 0 {
            scrollView.setContentOffset(CGPoint( x: scrollView.contentOffset.x - scrollView.frame.width , y: self.scrollView.frame.origin.y), animated: true)
            selectedImageViewIndex -= 1
                print(selectedImageViewIndex)
                let nextImageView = images[selectedImageViewIndex]
                nextImageView.frame = CGRect(x: nextImageView.frame.origin.x, y: nextImageView.frame.origin.y, width: nextImageView.frame.size.width + 30.0 , height: nextImageView.frame.size.height + 30.0)
                let previousImageView = images[selectedImageViewIndex+1]
                
                previousImageView.frame = CGRect(x: previousImageView.frame.origin.x, y: previousImageView.frame.origin.y, width: previousImageView.frame.size.width - 30.0 , height: previousImageView.frame.size.height - 30.0)
            }
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        self.scrollView.isScrollEnabled = false
 
    }
    
    func BalloonAnimation() {
        
        if (Animation == "ON"){

        UIView.animate(withDuration: 0.6,
                       animations: {
                        self.GeneratorEditButton.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
                        self.RightGeneratorEditButton.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
                        self.LeftGeneratorEditButton.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
        },
                       completion: { _ in
                        UIView.animate(withDuration: 0.6) {
                            self.GeneratorEditButton.transform = CGAffineTransform.identity
                            self.RightGeneratorEditButton.transform = CGAffineTransform.identity
                            self.LeftGeneratorEditButton.transform = CGAffineTransform.identity
                            
      }
     })
            
    }
            
        print("アニメーション処理")
    }
    
   

    func handleTap(_ sender: UITapGestureRecognizer) {
    
        let selectedImage = images[selectedImageViewIndex]
        
        if (sender.location(in: self.scrollView).x >= selectedImage.frame.origin.x && sender.location(in: self.scrollView).x <= selectedImage.frame.origin.x + selectedImage.frame.width && sender.location(in: self.scrollView).y >= selectedImage.frame.origin.y && sender.location(in: self.scrollView).y <= selectedImage.frame.origin.y + selectedImage.frame.height){

            let alert = UIAlertController(title: "画像番号 \(selectedImageViewIndex + 0)", message: "このアラートを閉じた後に画面下のボタンから編集してください", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            
            self.present(alert, animated: true, completion:nil)

            if(selectedImageViewIndex + 0 == 1){

            }

        }

    }
}

