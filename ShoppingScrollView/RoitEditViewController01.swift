import UIKit
import Social
import Photos

var ChangeBookObiGlobal:UIImage = UIImage(named:"animation1")!
var CheckChangeBookObiGlobal:UIImage = UIImage(named:"animation1")!
var fontSizeName = fontSizeNameSeting
var name = UIFont(name: "Menlo-BoldItalic" , size: CGFloat(fontSizeName))!
var coler = UIColor.black
var a1:UIImage = UIImage(named:"animation1")!
var a2:UIImage = UIImage(named:"animation2")!
var ShareToImage: UIImage?


var dropx = 220
var dropy = 260


class RoitEditViewController01: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    
    @IBOutlet weak var FacebookButton: UIButton!
    @IBOutlet weak var TwitterButton: UIButton!
    @IBOutlet weak var LineButton: UIButton!
    @IBOutlet weak var myCollectionView: UICollectionView!
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        print("collectionViewデータの個数を返すメソッド")
        return 9
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TestCell", for: indexPath as IndexPath) as UICollectionViewCell
        
        switch indexPath.row {
        case 1:
            cell.backgroundColor = .red
        case 2:
            cell.backgroundColor = .blue
        case 3:
            cell.backgroundColor = .orange
        case 4:
            cell.backgroundColor = .yellow
        case 5:
            cell.backgroundColor = .black
        case 6:
            cell.backgroundColor = .white
        case 7:
            cell.backgroundColor = .magenta
        case 8:
            cell.backgroundColor = .brown
        case 9:
            cell.backgroundColor = .darkGray
        default: break

        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if(indexPath.row == 1){
            coler = UIColor.red
        }
        if(indexPath.row == 2){
            coler = UIColor.blue
        }
        if(indexPath.row == 3){
            coler = UIColor.orange
        }
        if(indexPath.row == 4){
            coler = UIColor.yellow
        }
        if(indexPath.row == 5){
            coler = UIColor.black
        }
        if(indexPath.row == 6){
            coler = UIColor.white
        }
        if(indexPath.row == 7){
            coler = UIColor.magenta
        }
        if(indexPath.row == 8){
            coler = UIColor.brown
        }
        if(indexPath.row == 9){
            coler = UIColor.darkGray
        }
          rendering()
    }
    
    @IBAction func BookCoverButton(_ sender: Any) {
        let storyboard: UIStoryboard = UIStoryboard(name: "BookCoverRendering", bundle: nil)
        let nextView = storyboard.instantiateInitialViewController()
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        view.window!.layer.add(transition, forKey: kCATransition)
        present(nextView!, animated: false, completion: nil)
    }

    @IBAction func ShareToFacebook(_ sender: Any) {
        
        let twitterPostView:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)!
        let tweetDescription1:String = "#ルポ・メーカー"
        let tweetDescription2:String = "http://www.lamplight.co.jp/"
        let tweetURL:NSURL = NSURL(string: "https://itunes.apple.com/jp/developer/masaki-horimoto/id1018825942")!
        twitterPostView.setInitialText("\(tweetDescription1)\n\(tweetDescription2)")
        twitterPostView.add(tweetURL as URL!)
        twitterPostView.add(ShareToImage)
        self.present(twitterPostView, animated: true, completion: nil)
        
        if (sound == "あり"){
            print("効果音１")
            // システムサウンドへのパスを指定
            if let soundUrl:NSURL = NSURL(fileURLWithPath: "/Users/ryosukehujisawa/Desktop/BookCoverApp/sound/next.mp3") {
                
                // SystemsoundIDを作成して再生実行
                AudioServicesCreateSystemSoundID(soundUrl, &soundId)
                AudioServicesPlaySystemSound(soundId)
            }
        }
        

    }

    
    @IBAction func ShareToTwitter(_ sender: UIButton) {
        let twitterPostView:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeTwitter)!
        let tweetDescription1:String = "#ルポ・メーカー"
        let tweetDescription2:String = "http://www.lamplight.co.jp/"
        let tweetURL:NSURL = NSURL(string: "https://itunes.apple.com/jp/developer/masaki-horimoto/id1018825942")!
        twitterPostView.setInitialText("\(tweetDescription1)\n\(tweetDescription2)")
        twitterPostView.add(tweetURL as URL!)
        twitterPostView.add(ShareToImage)
        self.present(twitterPostView, animated: true, completion: nil)
        
        if (sound == "あり"){
            print("効果音１")
            // システムサウンドへのパスを指定
            if let soundUrl:NSURL = NSURL(fileURLWithPath: "/Users/ryosukehujisawa/Desktop/BookCoverApp/sound/next.mp3") {
                
                // SystemsoundIDを作成して再生実行
                AudioServicesCreateSystemSoundID(soundUrl, &soundId)
                AudioServicesPlaySystemSound(soundId)
            }
        }
        
    }
    
    
    
    @IBAction func ShareToLine(_ sender: UIButton) {
        let pastBoard: UIPasteboard = UIPasteboard.general
        
        pastBoard.setData(UIImageJPEGRepresentation(ShareToImage!, 1.0)!, forPasteboardType: "public.png")
        
        let lineUrlString: String = String(format: "line://msg/image/%@", pastBoard.name as CVarArg)
        
        UIApplication.shared.openURL(NSURL(string: lineUrlString)! as URL)
        
        if (sound == "あり"){
            print("効果音１")
            // システムサウンドへのパスを指定
            if let soundUrl:NSURL = NSURL(fileURLWithPath: "/Users/ryosukehujisawa/Desktop/BookCoverApp/sound/next.mp3") {
                
                // SystemsoundIDを作成して再生実行
                AudioServicesCreateSystemSoundID(soundUrl, &soundId)
                AudioServicesPlaySystemSound(soundId)
            }
        }
    }
    
    
    
    
    @IBAction func Setting(_ sender: Any) {
        let storyboard: UIStoryboard = UIStoryboard(name: "Setting", bundle: nil)
        let nextView = storyboard.instantiateInitialViewController()
        present(nextView!, animated: true, completion: nil)
        print("設定画面に遷移した")
            }
    
    var i:Int = 0
    var salarymanArr: Array = ["太めフォント" ,"細めフォント","普通フォント","筆フォント"]
    var fontArray = ["Menlo-BoldItalic","AmericanTypewriter-CondensedLight","Kailasa","HiraMinProN-W6"]
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return salarymanArr.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return salarymanArr[row] as? String
    }

    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 50))
        label.textAlignment = .center
        label.text = salarymanArr[row]
        label.font = UIFont(name: fontArray[row],size:20)
        return label
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {


        if (salarymanArr[row] == "太めフォント"){
            name = UIFont(name: "Menlo-BoldItalic" , size: CGFloat(fontSizeName))!
        }

        if (salarymanArr[row] == "細めフォント"){
            name = UIFont(name: "AmericanTypewriter-CondensedLight" , size: CGFloat(fontSizeName))!
        }
        
        if (salarymanArr[row] == "普通フォント"){
            name = UIFont(name: "Kailasa" , size: CGFloat(fontSizeName))!
        }
        
        if (salarymanArr[row] == "筆フォント"){
            name = UIFont(name: "HiraMinProN-W6" , size: CGFloat(fontSizeName)
                )!
        }
        
          rendering()
    }
    
    var selectedImageViewIndex  = 0
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var TextView: UITextView!
    @IBOutlet weak var NasIKnowICan: UIImageView!
    @IBOutlet weak var ChangeBookObi: UIImageView!
    @IBOutlet weak var ChoiceFontPicker: UIPickerView!
    @IBAction func CleatextButton(_ sender: UIButton) {
        
        textField.text! = ""
    }
    @IBAction func ChangeBookObiAction(_ sender: Any) {
        pickImageFromLibrary()
        rendering()
    }

    func pickImageFromLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            let controller = UIImagePickerController()
            controller.delegate = self
            controller.sourceType = UIImagePickerControllerSourceType.photoLibrary
            self.present(controller, animated: true, completion: nil)
            
            
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            ChangeBookObiGlobal = image
            
        } else{
        }
        
        self.dismiss(animated: true, completion: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()



        if(Lupo == "なし"){
            Lupo == "なし"
        }else if(Lupo == "あり"){
            Lupo = "あり"
        }
        UserDefaults.standard.set(Lupo, forKey: Constants.LupoKey)
        TableSubtitle[1][2] = Lupo
        
        
        
        
        
        
        
        
        if(sound == "なし"){
            sound = "なし"
        }else if(sound == "あり"){
            sound = "あり"
        }
        UserDefaults.standard.set(sound, forKey: Constants.soundKey)
        TableSubtitle[0][1] = sound
        
        
        
        
        
        

        
        if(fontSize == "中"){
            fontSize = "中"
        }else if(fontSize == "大"){
            fontSize = "大"
        }else if(fontSize == "小"){
            fontSize = "小"
        }
        UserDefaults.standard.set(fontSize, forKey: Constants.fontSizeKey)
        TableSubtitle[1][1] = fontSize
        fontSizeNameSeting = 70
        
        
        
        

        print(fontSizeNameSeting)
        if(Animation  == "OFF"){
        Animation  = "OFF"
        }else if(Animation  == "OM"){
        Animation  = "ON"
        }
        UserDefaults.standard.set(Animation, forKey: Constants.animationKey)
        TableSubtitle[0][2] = Animation


        
    
        
        myCollectionView.delegate = self
        myCollectionView.dataSource = self

        TwitterButton.imageView?.contentMode = .scaleAspectFill
        TwitterButton.imageView?.contentMode = UIViewContentMode.scaleAspectFill
        LineButton.imageView?.contentMode = .scaleAspectFill
        LineButton.imageView?.contentMode = UIViewContentMode.scaleAspectFill

        ChoiceFontPicker.delegate = self
        textField.layer.borderWidth = 4
        textField.layer.borderColor = UIColor.gray.cgColor
        NasIKnowICan.layer.borderWidth = 4
        NasIKnowICan.layer.borderColor = UIColor.gray.cgColor
        

        let button = UIButton()
        button.setTitle("ボタンのテキスト", for: .normal)
        button.setTitleColor(UIColor.blue, for: .normal)
        button.addTarget(self, action: #selector(buttonEvent(sender:)), for: .touchUpInside)
        button.sizeToFit()
        button.center = self.view.center
        self.view.addSubview(button)
        let imageEmmy:UIImage! = globalSelectedImageViewIndex
        let imageWidth = imageEmmy.size.width
        let imageHeight = imageEmmy.size.height
        let rect = CGRect(x:0, y:0, width:imageWidth, height:imageHeight)
        UIGraphicsBeginImageContext(imageEmmy.size)
        imageEmmy.draw(in: rect)
        let text = ""
        let font = UIFont.boldSystemFont(ofSize: 40)
        let textRect:CGRect = globalSelectedImageViewIndex == a1 ? CGRect(x:200, y:273, width:520, height:700) : CGRect(x:300, y:80, width:320, height:120)

        
        let textStyle = NSMutableParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
   
        let textFontAttributes = [
            NSFontAttributeName: font,
            NSForegroundColorAttributeName: UIColor.red,
            NSParagraphStyleAttributeName: textStyle
        ]
        
        text.draw(in: textRect, withAttributes: textFontAttributes)
        let newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext()
        NasIKnowICan.image = newImage
        self.view.addSubview(NasIKnowICan)
        textField.center.x = 270
        textField.center.y = 700
        textField.delegate = self
        textField.placeholder = "入力後、画像をタップで保存！"
        textField.backgroundColor = UIColor.clear
        textField.leftViewMode = .always
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        textField.returnKeyType = .done
        self.view.addSubview(textField)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        
        if(UserDefaults.standard.string(forKey: Constants.fontSizeKey)! == "大" ) {
            fontSizeName = 150
        }
        if(UserDefaults.standard.string(forKey: Constants.fontSizeKey)! == "中" ) {
            fontSizeName = 70
        }
        if(UserDefaults.standard.string(forKey: Constants.fontSizeKey)! == "小" ){
            print("小さい")
            fontSizeName = 40
        }
        name = UIFont(name: "Kailasa" , size: CGFloat(fontSizeName))!
        rendering()
        
        
        
        
        
        if (Animation == "ON"){
        UIView.animate(withDuration: 0.6,
                       animations: {
                        self.TwitterButton.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
                        self.LineButton.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
                        self.FacebookButton.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
                        self.NasIKnowICan.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
        },
                       completion: { _ in
                        UIView.animate(withDuration: 0.6) {
                        self.TwitterButton.transform = CGAffineTransform.identity
                        self.LineButton.transform = CGAffineTransform.identity
                        self.FacebookButton.transform = CGAffineTransform.identity
                        self.NasIKnowICan.transform = CGAffineTransform.identity
        }
       })
        print("アニメーション処理稼働")
      }

        rendering()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }

    func textFieldShouldClear(_ textField: UITextField) -> Bool {

        return true
    }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }

    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        rendering()
        return true
    }

    func buttonEvent(sender: UIButton) {

    }

    func saveImage(_ sender: UITapGestureRecognizer) {
        let targetImageView = sender.view! as! UIImageView
        let targetImage = targetImageView.image!
        UIImageWriteToSavedPhotosAlbum(targetImage, self, #selector(self.showResultOfSaveImage(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    func showResultOfSaveImage(_ image: UIImage, didFinishSavingWithError error: NSError!, contextInfo: UnsafeMutableRawPointer) {
        
        var title = "保存完了"
        var message = "カメラロールに保存しました"
        
        if error != nil {
            title = "エラー"
            message = "保存に失敗しました"
            
            

            if (sound == "あり"){
                print("効果音")
                // システムサウンドへのパスを指定
                if let soundUrl:NSURL = NSURL(fileURLWithPath: "/Users/ryosukehujisawa/Desktop/BookCoverApp/sound/save.mp3") {
                    
                    // SystemsoundIDを作成して再生実行
                    AudioServicesCreateSystemSoundID(soundUrl, &soundId)
                    AudioServicesPlaySystemSound(soundId)
                }
            }
            
            
            
        }
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    @IBAction func BackViewImage(_ sender: Any) {
        let storyboardBackViewImage = UIStoryboard(name: "Main", bundle: nil)
        let mainVCBackViewImage = storyboardBackViewImage.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        mainVCBackViewImage.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(mainVCBackViewImage, animated: true, completion: nil)
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touchEvent = touches.first!
        
        
        let newDx = touchEvent.location(in: self.view).x
        let newDy = touchEvent.location(in: self.view).y
        
         dropx = Int(newDx)
         dropy = Int(newDy)
  
        rendering()
        print("画像を移動させました")
    }

    func rendering() {
        
        if(true){
            let screenWidth:CGFloat = view.frame.size.width
            let screenHeight:CGFloat = view.frame.size.height
            let imageEmmy:UIImage! = globalSelectedImageViewIndex
            let imageWidth = imageEmmy.size.width
            let imageHeight = imageEmmy.size.height
            let rect = CGRect(x:0, y:0, width:imageWidth, height:imageHeight)
            let ChangeBookObiGlobalhensu:UIImage! = ChangeBookObiGlobal
            let imageWidth02 = ChangeBookObiGlobalhensu.size.width
            let imageHeight02 = ChangeBookObiGlobalhensu.size.height
            let rect02 = CGRect(x:0, y:560, width:600, height:300)
            
            UIGraphicsBeginImageContext(imageEmmy.size)
            imageEmmy.draw(in: rect)
            if(ChangeBookObiGlobal != CheckChangeBookObiGlobal){
            ChangeBookObiGlobalhensu.draw(in: rect02)

        }else{
                
        }
            let text = textField.text!
            _ = UIFont.boldSystemFont(ofSize: 40)
            
            let textRect  = CGRect(x:dropx, y:dropy, width:400, height:300)
            let textStyle = NSMutableParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
            let textFontAttributes = [
                NSFontAttributeName: name,
                NSForegroundColorAttributeName: coler,
                NSParagraphStyleAttributeName: textStyle
                ] as [String : Any]
            text.draw(in: textRect, withAttributes: textFontAttributes)
            let newImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext()
            NasIKnowICan.image = newImage
            let scale = screenWidth*1 / imageWidth
            _ = CGRect(x:0, y:100, width:imageWidth*scale, height:imageHeight*scale)
            ShareToImage = newImage
            self.view.addSubview(NasIKnowICan)
            NasIKnowICan.isUserInteractionEnabled = true
            NasIKnowICan.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.saveImage(_:))))
     }
    }
   }

