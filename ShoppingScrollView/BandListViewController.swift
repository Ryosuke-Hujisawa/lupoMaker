//
//  BandListViewController.swift
//  RiotGenerator
//
//  Created by ryosuke-hujisawa on 2017/05/12.
//  Copyright © 2017年 Mac. All rights reserved.
//

import UIKit

class BandListViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func BackViewBandToMain(_ sender: Any) {
        
        //ストーリーボードをインスタンス化
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextView = storyboard.instantiateInitialViewController()
        
        //右に遷移する
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionPush
        
        //kCATransitionFromLeftにすれば左に遷移します
        transition.subtype = kCATransitionFromLeft
        view.window!.layer.add(transition, forKey: kCATransition)
        present(nextView!, animated: false, completion: nil)
    }
    

}
