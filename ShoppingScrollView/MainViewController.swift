import UIKit
import Social
//import QuartzCore



//2枚目の画像をレンダリングする用のグローバル変数
var ChangeBookObiGlobal:UIImage = UIImage(named:"animation1")!
var CheckChangeBookObiGlobal:UIImage = UIImage(named:"animation1")!


//グローバルな変数を定義
var name = UIFont(name: "Menlo-BoldItalic" , size: 70)!

//グローバルな変数を定義
var coler = UIColor.black

var a1:UIImage = UIImage(named:"animation1")!
var a2:UIImage = UIImage(named:"animation2")!

var ShareToImage: UIImage?



class MainViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var TwitterButton: UIButton!
    @IBOutlet weak var LineButton: UIButton!
    @IBOutlet weak var myCollectionView: UICollectionView!
    
    
    
    
    //collectionViewデータの個数を返すメソッド
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        print("collectionViewデータの個数を返すメソッド")
        return 9
    }
    
    
    //データを返すメソッド
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        //コレクションビューから識別子「TestCell」のセルを取得する。
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TestCell", for: indexPath as IndexPath) as UICollectionViewCell

        //セルの背景色を設定して文字の色を変える。
        
        switch indexPath.row {
            
        case 1:
            
            cell.backgroundColor = .red
            
            
        case 2:
            
            cell.backgroundColor = .blue
            
        case 3:
            
            cell.backgroundColor = .orange
            
        case 4:
            
            cell.backgroundColor = .yellow
            
        case 5:
            
            cell.backgroundColor = .black
            
        case 6:
            
            cell.backgroundColor = .white
            
        case 7:
            
            
            cell.backgroundColor = .magenta
            
        case 8:
            
            cell.backgroundColor = .brown
            
        case 9:
            
            cell.backgroundColor = .darkGray

        default: break

        }
        
        
        return cell
    }
    
 
    
    //セルをクリックしたら呼ばれる
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print("コレクションビューセルをクリックしたら呼ばれる")
        print("Num：\(indexPath.row) Section:\(indexPath.section)")
        
        
        if(indexPath.row == 1){
            
            coler = UIColor.red
  
        }
        
        if(indexPath.row == 2){
            
            coler = UIColor.blue
  
        }
        
        if(indexPath.row == 3){
            
            coler = UIColor.orange

        }
        
        if(indexPath.row == 4){
            
            coler = UIColor.yellow
            
        }
        
        if(indexPath.row == 5){
            
            coler = UIColor.black

            
        }
        
        if(indexPath.row == 6){
            
            coler = UIColor.white
            
        }
        
        if(indexPath.row == 7){
            
            coler = UIColor.magenta
            
        }
        
        if(indexPath.row == 8){
            
            coler = UIColor.brown

            
        }
        
        if(indexPath.row == 9){
            
            coler = UIColor.darkGray
        }
        
        
        
          rendering()
        
        
    }
    

    
    @IBAction func BookCoverButton(_ sender: Any) {
        
        
        
        
        //ストーリーボードをインスタンス化
        let BandListstoryboard: UIStoryboard = UIStoryboard(name: "BandList", bundle: nil)
        let BandListView = BandListstoryboard.instantiateInitialViewController()
        
        //右に遷移する
        let BandListtransition = CATransition()
        BandListtransition.duration = 0.5
        BandListtransition.type = kCATransitionPush
        
        //kCATransitionFromLeftにすれば左に遷移します
        BandListtransition.subtype = kCATransitionFromRight
        view.window!.layer.add(BandListtransition, forKey: kCATransition)
        present(BandListView!, animated: false, completion: nil)
        
        
        print("反応してる＞")
        
        
        
    }
    
    
    
    
    
    
    
    @IBAction func ShareToFacebook(_ sender: Any) {
        
        //Tweet用のViewを作成する
        let twitterPostView:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)!
        
        let tweetDescription1:String = "#ルポ・メーカー"
        let tweetDescription2:String = "http://www.lamplight.co.jp/"
        let tweetURL:NSURL = NSURL(string: "https://itunes.apple.com/jp/developer/masaki-horimoto/id1018825942")!
        
        //Tweetする文章を設定する
        twitterPostView.setInitialText("\(tweetDescription1)\n\(tweetDescription2)")
        
        //Tweetに添付するURLを設定する
        twitterPostView.add(tweetURL as URL!)
        
        //起動時にキャプチャしたスクリーンショットを添付する
        twitterPostView.add(ShareToImage)
        
        //上述の内容を反映したTweet画面を表示する
        self.present(twitterPostView, animated: true, completion: nil)
        

    }
    
    
    
    
    
    

    //ツイッターへシェアする
    @IBAction func ShareToTwitter(_ sender: UIButton) {
        
        //Tweet用のViewを作成する
        let twitterPostView:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeTwitter)!
        
        let tweetDescription1:String = "#ルポ・メーカー"
        let tweetDescription2:String = "http://www.lamplight.co.jp/"
        let tweetURL:NSURL = NSURL(string: "https://itunes.apple.com/jp/developer/masaki-horimoto/id1018825942")!
        
        //Tweetする文章を設定する
        twitterPostView.setInitialText("\(tweetDescription1)\n\(tweetDescription2)")
        
        //Tweetに添付するURLを設定する
        twitterPostView.add(tweetURL as URL!)
        
        //起動時にキャプチャしたスクリーンショットを添付する
        twitterPostView.add(ShareToImage)
        
        //上述の内容を反映したTweet画面を表示する
        self.present(twitterPostView, animated: true, completion: nil)
        
    }
    
    
    
    @IBAction func ShareToLine(_ sender: UIButton) {
        //LINEにシェア
        //初期値が空っぽだと動かないよ多分
   
        let pastBoard: UIPasteboard = UIPasteboard.general
        
        pastBoard.setData(UIImageJPEGRepresentation(ShareToImage!, 1.0)!, forPasteboardType: "public.png")
        
        let lineUrlString: String = String(format: "line://msg/image/%@", pastBoard.name as CVarArg)
        
        UIApplication.shared.openURL(NSURL(string: lineUrlString)! as URL)

        
        
        
    }
    
    var i:Int = 0
    
    //var font = UIFont(name: "suusiki" , size: 100)
    //この配列の値に対してUIFontをつけたい

    var salarymanArr: Array = ["太めフォント" ,"細めフォント","普通フォント","筆フォント"]
    
    //salarymanArr[0] = UIFont.fontNames(forFamilyName: "suusiki")
    
    var fontArray = ["Menlo-BoldItalic","AmericanTypewriter-CondensedLight","IPAexGothic","HiraMinProN-W6"]

    
    //表示列
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    //表示個数
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return salarymanArr.count
    }
    
    //表示内容
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return salarymanArr[row] as? String
    }
    

    //fontを変える
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        // 表示するラベルを生成する
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 50))
        label.textAlignment = .center
        label.text = salarymanArr[row]
        label.font = UIFont(name: fontArray[row],size:20)
        return label
    }
    

    
    //選択時
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print("列: \(row)")
        print("値: \(salarymanArr[row])")

        //自動的にテキストフィールドを出す
        //textField.becomeFirstResponder()
        
        
        if (salarymanArr[row] == "太めフォント"){
            
            print("true")
            
            
            
            name = UIFont(name: "Menlo-BoldItalic" , size: 70)!

            
        }

        if (salarymanArr[row] == "細めフォント"){
            
            print("true")
            
            
            
            name = UIFont(name: "AmericanTypewriter-CondensedLight" , size: 70)!
     
            
        }
        
        if (salarymanArr[row] == "普通フォント"){
            
            print("true")
            
            
            
            name = UIFont(name: "IPAexGothic" , size: 70)!
            
            
        }
        
        if (salarymanArr[row] == "筆フォント"){
            
            print("true")
            
            
            
            name = UIFont(name: "HiraMinProN-W6" , size: 70)!
            
            
        }
        
        
          rendering()

        
    }
    
    var selectedImageViewIndex  = 0
    
    
    @IBOutlet weak var textField: UITextField!
    
    @IBOutlet weak var TextView: UITextView!
    
    @IBOutlet weak var NasIKnowICan: UIImageView!
    
    @IBOutlet weak var ChangeBookObi: UIImageView!
    
    @IBOutlet weak var ChoiceFontPicker: UIPickerView!

    
    @IBAction func CleatextButton(_ sender: UIButton) {
        
        
        //textField.font = UIFont(name: "suusiki", size: 50)
        
        textField.text! = ""

    }
    
    
    
    
    
    @IBAction func ChangeBookObiAction(_ sender: Any) {
        
        
        pickImageFromLibrary()  //ライブラリから写真を選択する
        
        rendering() //選択した写真をレンダリングする
        
        
    }

    
    
    
    
    
    
    
    /**
     ライブラリから写真を選択する
     */
    func pickImageFromLibrary() {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {    //追記
            
            //写真ライブラリ(カメラロール)表示用のViewControllerを宣言しているという理解
            let controller = UIImagePickerController()
            
            //おまじないという認識で今は良いと思う
            controller.delegate = self
            
            
            
            //新しく宣言したViewControllerでカメラとカメラロールのどちらを表示するかを指定
            //以下はカメラロールの例
            //.Cameraを指定した場合はカメラを呼び出し(シミュレーター不可)
            controller.sourceType = UIImagePickerControllerSourceType.photoLibrary

            
            //新たに追加したカメラロール表示ViewControllerをpresentViewControllerにする
            self.present(controller, animated: true, completion: nil)
            
            
        }
    }




    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
           // ChangeBookObi.image = image
            
            print("グローバル変数に2枚目のレンダリング用の画像を入れるよ")
           
            ChangeBookObiGlobal = image
            
        } else{
            print("Something went wrong")
        }
        
        self.dismiss(animated: true, completion: nil)
    }




    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        myCollectionView.delegate = self
        myCollectionView.dataSource = self
        
        
        
        
        //ChangeBookObi.image = UIImage(named:"animation1")!
        
        // view に ImageView を追加する
       // self.view.addSubview(ChangeBookObi)

        
        
        
        
        //ボタンの画像のサイズを調整する
        
        TwitterButton.imageView?.contentMode = .scaleAspectFill
        TwitterButton.imageView?.contentMode = UIViewContentMode.scaleAspectFill
        
        LineButton.imageView?.contentMode = .scaleAspectFill
        LineButton.imageView?.contentMode = UIViewContentMode.scaleAspectFill
        

        
        print(selectedImageViewIndex)
    
        ChoiceFontPicker.delegate = self
        
        textField.layer.borderWidth = 4
        textField.layer.borderColor = UIColor.gray.cgColor
        
        NasIKnowICan.layer.borderWidth = 4
        NasIKnowICan.layer.borderColor = UIColor.gray.cgColor
        
        
        // UIButtonのインスタンス
        let button = UIButton()
        
        // ボタンのテキスト設定
        button.setTitle("ボタンのテキスト", for: .normal)
        
        // ボタンのテキスト色を指定（これがないと白い文字になるため背景と同化して見えない）
        button.setTitleColor(UIColor.blue, for: .normal)
        
        // タップしたときに呼び出すメソッド指定
        button.addTarget(self, action: #selector(buttonEvent(sender:)), for: .touchUpInside)
        
        // 大きさの自動調節
        button.sizeToFit()
        
        // 位置を画面中央に
        button.center = self.view.center
        
        // ビューに追加
        self.view.addSubview(button)
        
       
        
        // Emmy画像の幅・高さの取得
        let imageWidth = imageEmmy.size.width
        let imageHeight = imageEmmy.size.height
        
        

        // 描画領域を生成
        let rect = CGRect(x:0, y:0, width:imageWidth, height:imageHeight)
        
        // 描画領域を生成
        //let rect = CGRect(x:0, y:0, width:imageWidth, height:imageHeight)
        
        
        

        UIGraphicsBeginImageContext(imageEmmy.size)
        // Retinaで画像が粗い場合
        //UIGraphicsBeginImageContextWithOptions(imageEmmy.size, false, 0)

        
        // EmmyをUIImageのdrawInRectメソッドでレンダリング
        imageEmmy.draw(in: rect)

        //ここに文字が挿入されるよ
        let text = ""

        let font = UIFont.boldSystemFont(ofSize: 40)
        
        // テキストの描画領域
        let textRect:CGRect = selectedImageViewIndex == a1 ? CGRect(x:200, y:273, width:520, height:700) : CGRect(x:300, y:80, width:320, height:120)

        
        let textStyle = NSMutableParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
   
        let textFontAttributes = [
            NSFontAttributeName: font,
            NSForegroundColorAttributeName: UIColor.red,
            NSParagraphStyleAttributeName: textStyle
        ]
        

        // テキストをdrawInRectメソッドでレンダリング
        text.draw(in: textRect, withAttributes: textFontAttributes)

        
        // Context に描画された画像を新しく設定
        let newImage = UIGraphicsGetImageFromCurrentImageContext();
        
        // Context 終了
        UIGraphicsEndImageContext()
        
        // UIImageView インスタンス生成
        //let imageView = UIImageView()
        NasIKnowICan.image = newImage


        // view に ImageView を追加する
        self.view.addSubview(NasIKnowICan)
        
        // 位置設定self.view.center.x
        textField.center.x = 270
        textField.center.y = 700
        
        // Delegate を設定
        textField.delegate = self
        
        // プレースホルダー
        textField.placeholder = "入力後、画像をタップで保存！"
        
        // 背景色
        textField.backgroundColor = UIColor.clear
        // 左の余白
        textField.leftViewMode = .always
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        
//        // テキストを全消去するボタンを表示
//        textField.clearButtonMode = .always
        
        // 改行ボタンの種類を変更
        textField.returnKeyType = .done
        
        // 画面に追加
        self.view.addSubview(textField)
    }
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("画面が呼び出された直後")
        
        print("画面が呼び出された直後にレンダリングする")
        rendering()
    }
    
    

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    /* 以下は UITextFieldDelegate のメソッド */
    // 改行ボタンを押した時の処理
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        // キーボードを隠す
        textField.resignFirstResponder()
        return true
    }
    
    // クリアボタンが押された時の処理
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        print("Clear")
        return true
    }
    
    
    
    // テキストフィールドがフォーカスされた時の処理
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("Start")
        return true
    }

    
    
    // テキストフィールドでの編集が終わろうとするときの処理
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        print(textField.delegate = self)


                rendering()


        return true
    }
    
    
    
    

    
    
    
    
    
    
    
    

    // ボタンが押された時に呼ばれるメソッド
    func buttonEvent(sender: UIButton) {
        print("ボタンが押された")
        print("このメソッドを呼び出したボタンの情報: \(sender)")

    }
    

    
    // セーブを行う
    func saveImage(_ sender: UITapGestureRecognizer) {
        
        // クリックした UIImageView を取得
        let targetImageView = sender.view! as! UIImageView
        
        // その中の UIImage を取得
        let targetImage = targetImageView.image!
        
        // UIImage の画像をカメラロールに画像を保存
        UIImageWriteToSavedPhotosAlbum(targetImage, self, #selector(self.showResultOfSaveImage(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    // 保存を試みた結果をダイアログで表示
    func showResultOfSaveImage(_ image: UIImage, didFinishSavingWithError error: NSError!, contextInfo: UnsafeMutableRawPointer) {
        
        var title = "保存完了"
        var message = "カメラロールに保存しました"
        
        if error != nil {
            title = "エラー"
            message = "保存に失敗しました"
        }
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        // OKボタンを追加
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        // UIAlertController を表示
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    
    
    
    

    
    
    
    
    @IBAction func BackViewImage(_ sender: Any) {
        
        
       
        
        
    }
    
    
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    func rendering() {
        
        
        if(true){
            
            print(textField.text!)
            
            // Screen Size の取得
            let screenWidth:CGFloat = view.frame.size.width
            let screenHeight:CGFloat = view.frame.size.height
            
            
            // Emmy画像の UIImage インスタンスを生成
            let imageEmmy:UIImage! = selectedImageViewIndex
            
            
            // Emmy画像の幅・高さの取得
            let imageWidth = imageEmmy.size.width
            let imageHeight = imageEmmy.size.height
            
            
            // ここで指定した大きさで画面に画像が現れます
            //let rect = CGRect(x:0, y:0, width:2000, height:1000)
            
            
            // 描画領域を生成
            let rect = CGRect(x:0, y:0, width:imageWidth, height:imageHeight)
            
            
            
            
            
            
            
            
            
            
            
            //ChangeBookObiGlobal
            // ChangeBookObiGlobal画像の UIImage インスタンスを生成
            let ChangeBookObiGlobalhensu:UIImage! = ChangeBookObiGlobal
            
            // Emmy画像の幅・高さの取得
            let imageWidth02 = ChangeBookObiGlobalhensu.size.width
            let imageHeight02 = ChangeBookObiGlobalhensu.size.height
            
            // ここで指定した大きさで画面に画像が現れます
            //let rect = CGRect(x:0, y:0, width:2000, height:1000)
            // 描画領域を生成
            let rect02 = CGRect(x:0, y:560, width:600, height:300)
            

            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
    
            /**
             
             
             
                 レンダリング開始。この中に書いていく
 
 
 
            */
            UIGraphicsBeginImageContext(imageEmmy.size)
            
            
            
            // EmmyをUIImageのdrawInRectメソッドでレンダリング
            imageEmmy.draw(in: rect)
            
            
            
            
            
            if(ChangeBookObiGlobal != CheckChangeBookObiGlobal){
            
                // 2枚目の画像をUIImageのdrawInRectメソッドでレンダリング
                
                
            ChangeBookObiGlobalhensu.draw(in: rect02)
                
                
                print("レンダリングしました")

            
            
        }else{
                
            print("更新用の画像は挿入されていません")
                
        }
        
            
            //textField2のtextにtextField.text!を入れて
            //textField2のtextField2.text!をvar text =に入れる
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            let text = textField.text!
            //let font = UIFont.boldSystemFont(ofSize: 120)
            
            _ = UIFont.boldSystemFont(ofSize: 40)
            //textField.font = UIFont(name: "suusiki", size: 100)
            
            
            //テキストの描画領域
            //これは必要
            let textRect  = CGRect(x:220, y:260, width:400, height:300)
            
            let textStyle = NSMutableParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
            
            let textFontAttributes = [
                NSFontAttributeName: name,
                NSForegroundColorAttributeName: coler,
                NSParagraphStyleAttributeName: textStyle
                ] as [String : Any]
            
            // テキストをdrawInRectメソッドでレンダリング
            text.draw(in: textRect, withAttributes: textFontAttributes)
            
            // Context に描画された画像を新しく設定
            let newImage = UIGraphicsGetImageFromCurrentImageContext();
            
            // Context 終了
            UIGraphicsEndImageContext()
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            // UIImageView インスタンス生成
            //let imageView = UIImageView()
            
            NasIKnowICan.image = newImage
            
            
            //ここを１にすることでオートレイアウトできる
            // 画像サイズをスクリーン幅に合わせる, scaling
            let scale = screenWidth*1 / imageWidth
            _ = CGRect(x:0, y:100, width:imageWidth*scale, height:imageHeight*scale)
            
            
            ShareToImage = newImage
            
            
            self.view.addSubview(NasIKnowICan)
            
            
            // UIImageView がタップできるようにイベント追加
            NasIKnowICan.isUserInteractionEnabled = true
            NasIKnowICan.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.saveImage(_:))))
            
            
            
            
            
        }
    }
    
    
    
    
    
    
    
    
   }

