import UIKit
var fontSizeNameSeting = 70









var Lupo = "あり"
let fontAngle = "0"
var fontSize = "中"
var Animation = "ON"
var sound = "あり"

struct Constants {
    static let LupoKey = "LupoKey"
    static let fontAngleKey = "fontAngle"
    static let fontSizeKey = "fontSize"
    static let animationKey = "Animation"
    static let soundKey = "sound"
}

var TableTitle = [ ["効果", "効果音", "アニメーション"],
                   ["レイアウト初期設定", "文字サイズ", "ルポ"],
                   
                   
                   
                   
                   
                   
                   
                   ["アプリの使い方", "遊び方", "注意点"],
                   ["その他", "アプリ情報"]]








var TableSubtitle = [ ["効果", UserDefaults.standard.string(forKey: Constants.soundKey), UserDefaults.standard.string(forKey: Constants.animationKey)],
                      
                      ["レイアウト初期設定", UserDefaults.standard.string(forKey: Constants.fontSizeKey), UserDefaults.standard.string(forKey: Constants.LupoKey)],
                      
                      
                      
                      
                      
                      ["アプリの使い方", "", ""],
                      ["その他", ""]]







class TableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        UserDefaults.standard.set(Lupo, forKey: Constants.LupoKey)
        UserDefaults.standard.set(fontAngle, forKey: Constants.fontAngleKey)
        UserDefaults.standard.set(fontSize, forKey: Constants.fontSizeKey)
        UserDefaults.standard.set(Animation, forKey: Constants.animationKey)
        UserDefaults.standard.set(sound, forKey: Constants.soundKey)
        }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func BackViewButton(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        // セクション(グループの数)を返す　この場合arrayは設定 1〜3の３つのセクション
        return TableTitle.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        // セクションに入るセルの数を返す　-1 しているのは各セクションの一つ目の要素がタイトルヘッダーとなるためそれを除いている
        return TableTitle[section].count - 1
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "cell")
        
        //セルを取得する。
        //let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath:indexPath) as UITableViewCell
        
        
        //セルを取得する。
        //let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for:indexPath) as UITableViewCell
        

        //let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        cell.textLabel?.text = TableTitle[indexPath.section][indexPath.row + 1]
        
        
        cell.detailTextLabel?.text = TableSubtitle[indexPath.section][indexPath.row + 1]
        
        return cell
    }
    
     func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        //　タイトルヘッダーの要素を返している　今回は各セクションの先頭 0番目がヘッダーとなる
        return TableTitle[section][0]
    }
    
    
    
    
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // セレクトされた後にセルのハイライトを消している
        

        tableView.deselectRow(at: indexPath, animated: false)
        if indexPath.section == 0 {
            
                if(indexPath.row == 0){
                    print("効果音")
                    if (UserDefaults.standard.string(forKey: Constants.soundKey)! == "あり")
                    {
                        sound = "なし"
                        UserDefaults.standard.set(sound, forKey: Constants.soundKey)
                        TableSubtitle[0][1] = sound
                        tableView.reloadData()
                    }else if (UserDefaults.standard.string(forKey: Constants.soundKey)! == "なし"){
                        sound  = "あり"
                        UserDefaults.standard.set(sound, forKey: Constants.soundKey)
                        TableSubtitle[0][1] = sound
                        tableView.reloadData()
                    }
            }

            if(indexPath.row == 1){
                print("アニメーション")
                
                if (UserDefaults.standard.string(forKey: Constants.animationKey)! == "ON"){
                    Animation = "OFF"
                    UserDefaults.standard.set(Animation, forKey: Constants.animationKey)
                    TableSubtitle[0][2] = Animation
                    print(TableSubtitle)
                    tableView.reloadData()
                }else if (UserDefaults.standard.string(forKey: Constants.animationKey)! == "OFF"){
                    Animation  = "ON"
                    UserDefaults.standard.set(Animation, forKey: Constants.animationKey)
                    TableSubtitle[0][2] = Animation
                    tableView.reloadData()
                }
            }
            print("タップされたアクセサリがあるセルのindex番号: \(indexPath.row)")
        } else if indexPath.section == 1 {
 
        if(indexPath.row == 0){
        if ( (UserDefaults.standard.string(forKey: Constants.fontSizeKey)!)  == "中" ){
            fontSize = "大"
            UserDefaults.standard.set(fontSize, forKey: Constants.fontSizeKey)
            TableSubtitle[1][1] = fontSize
            fontSizeNameSeting = 1500
            print(fontSizeNameSeting)
            tableView.reloadData()
            }else if ( (UserDefaults.standard.string(forKey: Constants.fontSizeKey)!)  == "大" ){
            fontSize = "小"
            UserDefaults.standard.set(fontSize, forKey: Constants.fontSizeKey)
            TableSubtitle[1][1] = fontSize
            fontSizeNameSeting = 40
            print(fontSizeNameSeting)
            tableView.reloadData()
            }else if ( (UserDefaults.standard.string(forKey: Constants.fontSizeKey)!)  == "小" ){
            fontSize = "中"
            UserDefaults.standard.set(fontSize, forKey: Constants.fontSizeKey)
            TableSubtitle[1][1] = fontSize
            fontSizeNameSeting = 70
            print(fontSizeNameSeting)
            tableView.reloadData()
                }
                
            }
    
            if(indexPath.row == 1){
            if (UserDefaults.standard.string(forKey: Constants.LupoKey)! == "あり"){
                Lupo = "なし"
                UserDefaults.standard.set(Lupo, forKey: Constants.LupoKey)
                TableSubtitle[1][2] = Lupo
                print(TableSubtitle)
                tableView.reloadData()
            }else if (UserDefaults.standard.string(forKey: Constants.LupoKey)! == "なし"){
                Lupo  = "あり"
                UserDefaults.standard.set(Lupo, forKey: Constants.LupoKey)
                TableSubtitle[1][2] = Lupo
                print(TableSubtitle)
                tableView.reloadData()  
            }
            }

        }else if indexPath.section == 2 {

            print("タップされたアクセサリがあるセルのindex番号: \(indexPath.row)")
            print("3だぜえ")
            
        }else if indexPath.section == 3 {

            print("タップされたアクセサリがあるセルのindex番号: \(indexPath.row)")
            print("4だぜえ")

            let storyboard: UIStoryboard = self.storyboard!
            let nextView = storyboard.instantiateViewController(withIdentifier: "ApplicationInformation")
            let transition = CATransition()
            transition.duration = 0.5
            transition.type = kCATransitionPush
            transition.subtype = kCATransitionFromRight
            view.window!.layer.add(transition, forKey: kCATransition)
            present(nextView, animated: false, completion: nil)
        }
    }
}
