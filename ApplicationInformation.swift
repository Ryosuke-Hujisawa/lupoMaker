import UIKit

class ApplicationInformation: UIViewController {
    
    
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var CompanyURL: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(ApplicationInformation.tapFunction))
        CompanyURL.isUserInteractionEnabled = true
        CompanyURL.addGestureRecognizer(tap)
        CompanyURL.textColor = UIColor.blue
        
        self.iconImage.layer.masksToBounds = true
        self.iconImage.layer.cornerRadius = 8
        //self.iconImage.layer.shadowOffset = CGSize(width: 0, height: -10)
        self.iconImage.layer.shadowRadius = 3
        self.iconImage.layer.shadowOpacity = 0.8
    }

    func tapFunction(sender:UITapGestureRecognizer) {
        print("tap working")
        let url = NSURL(string: "http://www.lamplight.co.jp/")
        if UIApplication.shared.canOpenURL(url! as URL){
            UIApplication.shared.openURL(url! as URL)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func BackButton(_ sender: Any) {
        
//        let storyboard: UIStoryboard = self.storyboard!
//        let nextView = storyboard.instantiateViewController(withIdentifier: "Setting")
        
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromLeft
        view.window!.layer.add(transition, forKey: kCATransition)
        self.dismiss(animated: false, completion: nil)
        //present(nextView, animated: false, completion: nil)

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
