//
//  SocialShareViewController.swift
//  RiotGenerator
//
//  Created by lamplight02 on 2017/03/14.
//  Copyright © 2017年 Mac. All rights reserved.
//

import UIKit
import Social



func SocialShare() {
    
    //Tweet用のViewを作成する
    let twitterPostView:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeTwitter)!
    
    let tweetDescription1:String = "アプリだぜ！"
    let tweetDescription2:String = "ツイッターにシェアするぜ！"
    let tweetURL:NSURL = NSURL(string: "https://itunes.apple.com/jp/developer/masaki-horimoto/id1018825942")!
    
    //Tweetする文章を設定する
    twitterPostView.setInitialText("\(tweetDescription1)\n\(tweetDescription2)")
    
    //Tweetに添付するURLを設定する
    twitterPostView.add(tweetURL as URL!)
    
    //起動時にキャプチャしたスクリーンショットを添付する
    twitterPostView.add(globalNewImage)
    
    //上述の内容を反映したTweet画面を表示する
    SocialShareViewController().present(twitterPostView, animated: true, completion: nil)
    
}





class SocialShareViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
